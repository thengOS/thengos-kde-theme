<img src="https://github.com/vinceliuice/Qogir-kde/blob/master/images/logo.png" alt="Logo" align="right" /> ThengOS KDE Theme forked from Qogir KDE Theme
======

ThengOS KDE is a theme forked from Qogir kde. This is a flat Design theme for KDE Plasma desktop.

This Theme is used as the default theme for [ThengOS](https://keralinux.com)

In this repository you'll find:

- Aurorae Themes
- Kvantum Themes
- Plasma Color Schemes
- Plasma Desktop Themes
- Plasma Look-and-Feel Settings

## Installation

```sh
./install.sh
```

## Recommendations

- For better looking please use this pack with [Kvantum engine](https://github.com/tsujan/Kvantum/tree/master/Kvantum).

  Run `kvantummanager` to choose and apply **Qogir-light** (or any other Qogir) theme.

- Install [Qogir icon theme](https://github.com/vinceliuice/Qogir-icon-theme) for a more consistent and beautiful experience.

## License

GNU GPL v3

## preview

![1](https://github.com/vinceliuice/Qogir-kde/blob/master/images/preview1.png?raw=true)
![2](https://github.com/vinceliuice/Qogir-kde/blob/master/images/preview2.png?raw=true)
![3](https://github.com/vinceliuice/Qogir-kde/blob/master/images/preview3.png?raw=true)

